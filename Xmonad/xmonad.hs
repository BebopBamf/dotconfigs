import System.Exit (ExitCode (ExitSuccess), exitSuccess, exitWith)
import System.IO ()
import XMonad (
    Choose,
    Default (def),
    KeyMask,
    KeySym,
    X,
    XConfig (
        XConfig,
        borderWidth,
        focusedBorderColor,
        layoutHook,
        modMask,
        terminal
    ),
    controlMask,
    io,
    mod4Mask,
    shiftMask,
    spawn,
    xK_F2,
    xK_Print,
    xK_Return,
    xK_b,
    xK_c,
    xK_d,
    xK_e,
    xK_l,
    xK_p,
    xK_q,
    xmonad,
    (.|.),
    (|||),
 )
import XMonad.Config.Desktop (desktopConfig)
import XMonad.Core (startupHook)
import XMonad.Hooks.DynamicLog (
    def,
    statusBar,
    wrap,
    xmobarColor,
    xmobarPP,
 )
import XMonad.Hooks.ManageDocks (AvoidStruts, avoidStruts)
import XMonad.Hooks.StatusBar.PP (PP, ppCurrent)
import XMonad.Layout.Spacing (Border (Border), spacingRaw)
import XMonad.Layout.ThreeColumns (ThreeCol (ThreeCol, ThreeColMid))
import XMonad.Operations (kill)
import XMonad.Prompt (
    XPConfig (font, height, position),
    XPPosition (Top),
    def,
 )
import XMonad.Prompt.ConfirmPrompt (confirmPrompt)
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.Run (spawnPipe)

myExitPromptConf :: XPConfig
myExitPromptConf =
    def
        { font = "xft:Iosevka Plex:regular"
        , position = Top
        , height = 24
        }

myPPConf :: PP
myPPConf =
    xmobarPP
        { ppCurrent = xmobarColor "#44475a" "" . wrap "<" ">"
        }

toggleStrutsKey :: XConfig c -> (KeyMask, KeySym)
toggleStrutsKey XConfig{modMask = modMask} = (modMask, xK_b)

myStartupHook :: X ()
myStartupHook = do
    spawn "~/.config/xmonad/scripts/startapps.sh"

myKeyConf :: [((KeyMask, KeySym), X ())]
myKeyConf =
    [ ((mod4Mask, xK_d), spawn "rlaunch")
    , ((mod4Mask, xK_Return), spawn "wezterm")
    , ((mod4Mask .|. shiftMask, xK_c), spawn "if type xmonad; then xmonad --recompile && xmonad --restart; else xmessage xmonad not in \\$PATH: \"$PATH\"; fi") -- %! Restart xmonad
    , ((mod4Mask .|. shiftMask, xK_e), confirmPrompt myExitPromptConf "Exit" $ io exitSuccess)
    , ((mod4Mask .|. shiftMask, xK_p), spawn "rofi -theme clean -show combi")
    , ((mod4Mask .|. shiftMask, xK_q), kill)
    , ((0, xK_Print), spawn "shotgun /home/emendoza/Pictures/Screenshots/screenshot_$(date +%Y-%m-%d_%H%M%S).png")
    , ((controlMask, xK_Print), spawn "/home/emendoza/.config/screenshot/screenshot_window.sh")
    , ((shiftMask, xK_Print), spawn "/home/emendoza/.config/screenshot/screenshot_selection.sh")
    , ((mod4Mask, xK_F2), spawn "firefox")
    ]

threeColLayout :: Choose ThreeCol ThreeCol a
threeColLayout = ThreeCol 1 (3 / 100) (1 / 2) ||| ThreeColMid 1 (3 / 100) (1 / 2)

myLayoutHook = avoidStruts . spacingRaw True (Border 0 10 10 10) True (Border 10 10 10 10) True $ threeColLayout ||| layoutHook def

myDefaultsConf =
    desktopConfig
        { terminal = "wezterm"
        , modMask = mod4Mask
        , borderWidth = 1
        , focusedBorderColor = "#f8f8f2"
        , startupHook = myStartupHook
        , layoutHook = myLayoutHook
        }

myConf = myDefaultsConf `additionalKeys` myKeyConf

main :: IO ()
main = xmonad =<< statusBar "xmobar" myPPConf toggleStrutsKey myConf

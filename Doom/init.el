;;; init.el -*- lexical-binding: t; -*-

;; This file controls what Doom modules are enabled and what order they load
;; in. Remember to run 'doom sync' after modifying it!

;; NOTE Press 'SPC h d h' (or 'C-h d h' for non-vim users) to access Doom's
;;      documentation. There you'll find a link to Doom's Module Index where all
;;      of our modules are listed, including what flags they support.

;; NOTE Move your cursor over a module's name (or its flags) and press 'K' (or
;;      'C-c c k' for non-vim users) to view its documentation. This works on
;;      flags as well (those symbols that start with a plus).
;;
;;      Alternatively, press 'gd' (or 'C-c c d') on a module to browse its
;;      directory (for easy access to its source code).

(doom! :input
       ;;bidi              ; (tfel ot) thgir etirw uoy gnipleh
       ;;chinese
       ;;japanese
       ;;layout            ; auie,ctsrnm is the superior home row

       :completion
       ;;(company
       ;; +childframe)       ; the ultimate code completion backend
       (vertico
        +icons)            ; the search engine of the future
       (corfu
        +icons
        +orderless)        ; complete with cap(f), cape and a flying feather!
       ;;helm              ; the *other* search engine for love and life
       ;;ido               ; the other *other* search engine...
       ;;ivy               ; a search engine for love and life

       :ui
       doom                ; what makes DOOM look the way it does
       doom-dashboard      ; a nifty splash screen for Emacs
       hl-todo             ; highlight TODO/FIXME/NOTE/DEPRECATED/HACK/REVIEW
       indent-guides       ; highlighted indent columns
       ligatures           ; ligatures and symbols to make your code pretty again
       ;;(ligatures +extra)  ; ligatures and symbols to make your code pretty again
       minimap             ; show a map of the code on the side
       modeline            ; snazzy, Atom-inspired modeline, plus API
       ophints             ; highlight the region an operation acts on
       (popup +defaults)   ; tame sudden yet inevitable temporary windows
       treemacs            ; a project drawer, like neotree but cooler
       ;;(treemacs +lsp)     ; a project drawer, like neotree but cooler
       unicode             ; extended unicode support for various languages
       (vc-gutter +pretty) ; vcs diff in the fringe
       vi-tilde-fringe     ; fringe tildes to mark beyond EOB
       workspaces          ; tab emulation, persistence & separate workspaces
       zen                 ; distraction-free coding or writing
       ;;(emoji +unicode)    ; 🙂
       ;;deft              ; notational velocity for Emacs
       ;;doom-quit         ; DOOM quit-message prompts when you quit Emacs
       ;;nav-flash         ; blink cursor line after big motions
       ;;neotree           ; a project drawer, like NERDTree for vim
       ;;tabs              ; a tab bar for Emacs
       ;;window-select     ; visually switch windows

       :editor
       (evil +everywhere)  ; come to the dark side, we have cookies
       file-templates      ; auto-snippets for empty files
       fold                ; (nigh) universal code folding
       (format +onsave)    ; automated prettiness
       snippets            ; my elves. They type so I don't have to
       word-wrap           ; soft wrapping with language-aware indent
       ;;god               ; run Emacs commands without modifier keys
       ;;lispy             ; vim for lisp, for people who don't like vim
       ;;multiple-cursors  ; editing in many places at once
       ;;objed             ; text object editing for the innocent
       ;;parinfer          ; turn lisp into python, sort of
       ;;rotate-text       ; cycle region at point between text candidates

       :emacs
       (dired
        +dirvish
        +icons)            ; making dired pretty [functional]
       electric            ; smarter, keyword-based electric-indent
       (ibuffer +icons)    ; interactive buffer management
       undo                ; persistent, smarter undo for your inevitable mistakes
       vc                  ; version-control and Emacs, sitting in a tree

       :term
       eshell            ; the elisp shell that works everywhere
       ;;shell             ; simple shell REPL for Emacs
       ;;term              ; basic terminal emulator for Emacs
       vterm             ; the best terminal emulation in Emacs

       :checkers
       syntax              ; tasing you for every semicolon you forget
       (spell
        +flyspell
        +enchant)          ; tasing you for misspelling mispelling
       ;;grammar             ; tasing grammar mistake every you make

       :tools
       biblio              ; Writes a PhD for you (citation needed)
       docker
       editorconfig        ; let someone else argue about tabs vs spaces
       (eval +overlay)     ; run code, run (also, repls)
       lookup
       ;;(lsp +peek)         ; M-x vscode
       (lsp +eglot)       ; M-x vscode
       magit               ; a git porcelain for Emacs
       make                ; run make tasks from Emacs
       pdf                 ; pdf enhancements
       tree-sitter         ; syntax and parsing, sitting in a tree...
       ;;ansible
       ;;collab            ; buffers with friends
       ;;debugger          ; FIXME stepping through code, to help you add bugs
       ;;direnv
       ;;ein               ; tame Jupyter notebooks with emacs
       ;;gist              ; interacting with github gists
       ;;pass              ; password manager for nerds
       ;;prodigy           ; FIXME managing external services & code builders
       ;;terraform         ; infrastructure as code
       ;;tmux              ; an API for interacting with tmux
       ;;upload            ; map local to remote projects via ssh/ftp

       :os
       (:if IS-MAC macos)  ; improve compatibility with macOS
       ;;tty               ; improve the terminal Emacs experience

       :lang
       (agda
        +local
        +tree-sitter)      ; types of types of types of types...
       (cc
        +lsp)              ; C > C++ == 1
       (clojure
        +lsp +tree-sitter) ; java with a lisp
       coq                 ; proofs-as-programs
       data                ; config/data formats
       ;; (elixir
       ;;  +lsp +tree-sitter) ; erlang done right
       ;; (elm
       ;;  +lsp +tree-sitter) ; care for a cup of TEA?
       emacs-lisp          ; drown in parentheses
       ;; (erlang
       ;;  +lsp +tree-sitter) ; an elegant language for a more civilized age
       ;;(graphql +lsp)      ; Give queries a REST
       (haskell +lsp)      ; a language that's lazier than I am
       (java +lsp)         ; the poster child for carpal tunnel syndrome
       (javascript +lsp)
       (json
        +tree-sitter)      ; At least it ain't XML
       ;;(javascript
       ;;  +lsp +tree-sitter) ; all(hope(abandon(ye(who(enter(here))))))
       ;;latex               ; writing papers in Emacs has never been so fun
       (latex
        ;;+fold
        +cdlatex)          ; writing papers in Emacs has never been so fun
       ledger              ; be audit you can be
       (lua
        +fennel
        +lsp
        +tree-sitter)      ; one-based indices? one-based indices
       markdown            ; writing docs for people to ignore
       ;;nix                 ; I hereby declare "nix geht mehr!"
       (ocaml
        +tree-sitter)      ; an objective camel
       (org
        ;; core features
        +crypt
        +dragndrop
        ;; utilities and visual
        ;; +pretty
        +pandoc
        +hugo
        +brain
        ;; extra modes
        ;; +journal
        +noter
        +pomodoro
        +present
        +roam2)            ; organize your plain life in plain text
       (python
        +conda
        +pyright
        +lsp
        +tree-sitter)      ; beautiful is better than ugly
       scala               ; java, but good
       (rest +jq)          ; Emacs as a REST client
       (rust
        +lsp)              ; Fe2O3.unwrap().unwrap().unwrap().unwrap()
       sh                  ; she sells {ba,z,fi}sh shells on the C xor
       web                 ; the tubes
       (yaml +tree-sitter) ; JSON, but readable

       ;;beancount         ; mind the GAAP
       ;;common-lisp       ; if you've seen one lisp, you've seen them all
       ;; (csharp
       ;;  +dotnet
       ;;  +lsp
       ;;  +tree-sitter)      ; unity, .NET, and mono shenanigans
       ;;crystal           ; ruby at the speed of c
       ;;(dart
       ;; +flutter +lsp)     ; paint ui and not much else
       ;;dhall
       ;;ess               ; emacs speaks statistics
       ;;factor
       ;;faust             ; dsp, but you get to keep your soul
       ;;fortran           ; in FORTRAN, GOD is REAL (unless declared INTEGER)
       ;;(fsharp +lsp)       ; ML stands for Microsoft's Language
       ;;fstar             ; (dependent) types and (monadic) effects and Z3
       ;;gdscript          ; the language you waited for
       ;;(go +lsp)         ; the hipster dialect
       ;;hy                ; readability of scheme w/ speed of python
       ;;idris             ; a language you can depend on
       ;;julia             ; a better, faster MATLAB
       ;;kotlin            ; a better, slicker Java(Script)
       ;;lean              ; for folks with too much to prove
       ;;nim               ; python + lisp at the speed of c
       ;;(purescript +lsp)   ; javascript, but functional
       ;;php               ; perl's insecure younger brother
       ;;plantuml          ; diagrams for confusing people more
       ;;qt                ; the 'cutest' gui framework ever
       ;;racket            ; a DSL for DSLs
       ;;raku              ; the artist formerly known as perl6
       ;;rst               ; ReST in peace
       ;;(ruby +rails)     ; 1.step {|i| p "Ruby is #{i.even? ? 'love' : 'life'}"}
       ;;(scheme +guile)   ; a fully conniving family of lisps
       ;;sml
       ;;solidity          ; do you need a blockchain? No.
       ;;(swift
       ;;+lsp +tree-sitter) ; who asked for emoji variables?
       ;;terra             ; Earth and Moon in alignment for performance.
       ;;zig               ; C, but simpler

       :email
       ;;(mu4e +org +gmail)
       (notmuch +afew +org)
       ;;(wanderlust +gmail)

       :app
       ;;calendar
       (rss +org)          ; emacs as an RSS reader
       ;;emms
       ;;everywhere        ; *leave* Emacs!? You must be joking
       ;;irc               ; how neckbeards socialize
       ;;twitter           ; twitter client https://twitter.com/vnought

       :config
       ;;literate
       (default +bindings +smartparens))

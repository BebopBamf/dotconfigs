local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require 'lazy'.setup {
  "udayvir-singh/tangerine.nvim",
  "udayvir-singh/hibiscus.nvim",
  "morhetz/gruvbox",
  {
    'nvim-telescope/telescope.nvim',
    dependencies = {'nvim-lua/plenary.nvim'}
  },
  "nvim-telescope/telescope-fzf-native.nvim",
}

local function bootstrap(url, ref)
  local name = url:gsub(".*/", "")
  local path = vim.fn.stdpath [[data]] .. "/lazy/" .. name

  if vim.fn.isdirectory(path) == 0 then
    print(name .. ": installing in data dir...")

    vim.fn.system { "git", "clone", url, path }
    if ref then
      vim.fn.system { "git", "-C", path, "checkout", ref }
    end

    vim.cmd [[redraw]]
    print(name .. ": finished installing")
  end
  vim.opt.runtimepath:prepend(path)
end

bootstrap("https://github.com/udayvir-singh/tangerine.nvim")
bootstrap("https://github.com/udayvir-singh/hibiscus.nvim")

require 'tangerine'.setup {
  target = vim.fn.stdpath [[data]] .. "/tangerine",

  rtpdirs = {
    "ftplugin",
  },

  compiler = {
    verbose = false,

    hooks = { "onsave", "oninit" }
  },
}



$pdf_previewer = 'start zathura';

$aux_dir = '.tex-cache';
$pdflatex = 'pdflatex -synctex=1 -interaction=nonstopmode';
@generated_exts = (@generated_exts, 'synctex.gz');

local wt = require 'wezterm';

return {
    font = wt.font 'Iosevka Plex Term',
    color_scheme = 'Dracula (Official)',
    enable_tab_bar = false,
    window_background_opacity = 1.0,
    hyperlink_rules = {
        -- Linkify things that look like URLs
        -- This is actually the default if you don't specify any hyperlink_rules
        {
            regex = "\\b\\w+://(?:[\\w.-]+)\\.[a-z]{2,15}\\S*\\b",
            format = "$0",
        },

        -- linkify email addresses
        {
            regex = "\\b\\w+@[\\w-]+(\\.[\\w-]+)+\\b",
            format = "mailto:$0",
        },

        -- file:// URI
        {
            regex = "\\bfile://\\S*\\b",
            format = "$0",
        },
        
        -- Linkify things that look like URLs with numeric addresses as hosts.
        -- E.g. http://127.0.0.1:8000 for a local development server,
        -- or http://192.168.1.1 for the web interface of many routers
        {
            regex = [[\b\w+://(?:[\d]{1,3}\.){3}[\d]{1,3}\S*\b]],
            format = '$0',
        },

        -- Linkify ip addresses
        {
            regex = [[\b(?:[\d]{1,3}\.){3}[\d]{1,3}\S*\b]],
            format = 'http://$0',
        },

        -- Linkify localhost 
        {
            regex = [[\blocalhost\S*\b]],
            format = 'http://$0',
        },
    },
}

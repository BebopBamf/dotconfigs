export SHELL_SESSIONS_DISABLE=1

# Configure XDG Directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export COPYFILE_DISABLE=1


################################################################################
# System Variables
################################################################################

export EDITOR="nvim"


################################################################################
# XDG Homes
################################################################################

# Haskell Related
export GHCUP_USE_XDG_DIRS=1
export STACK_XDG=1
export CABAL_CONFIG="$XDG_CONFIG_HOME"/cabal/config
export CABAL_DIR="$XDG_DATA_HOME"/cabal

# Mobile App Development
export ANDROID_HOME="$XDG_DATA_HOME/android/sdk"
export ANDROID_USER_HOME="$XDG_DATA_HOME/android"

# JAVA and related
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export LEIN_HOME="$XDG_DATA_HOME/lein"

# Dotnet
export NUGET_PACKAGES="$XDG_CACHE_HOME/NuGetPackages"

# Rust
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# NodeJS
export PNPM_HOME="$XDG_DATA_HOME/pnpm"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export NODE_REPL_HISTORY="$XDG_STATE_HOME/node/history"
export npm_config_prefix="$HOME/.local"
export ELM_HOME="$XDG_DATA_HOME/elm"
export VOLTA_HOME="$XDG_DATA_HOME/volta"
export VOLTA_FEATURE_PNPM=1

# Caml
export OPAMROOT="$XDG_DATA_HOME/opam"
export ESY__PREFIX="$XDG_DATA_HOME/esy"

# MAFS
export DOT_SAGE="$XDG_DATA_HOME/sage"
export AGDA_DIR="$XDG_DATA_HOME/agda"

# Ruby
export GEM_HOME="$XDG_DATA_HOME/gem"
export BUNDLE_USER_CONFIG="$XDG_CONFIG_HOME"/bundle
export BUNDLE_USER_CACHE="$XDG_CACHE_HOME"/bundle
export BUNDLE_USER_PLUGIN="$XDG_DATA_HOME"/bundle

# Python
export PYTHONHISTORY="$XDG_STATE_HOME/python/history"
export IPYTHONDIR="$XDG_CONFIG_HOME/ipython"
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"

# ASDF
export ASDF_CONFIG_FILE="$XDG_CONFIG_HOME/asdf/asdfrc"
export ASDF_DIR="$XDG_DATA_HOME/asdf"
export ASDF_DATA_DIR="$XDG_DATA_HOME/asdf"

# System Files
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export HISTFILE="$XDG_STATE_HOME/zsh/history"
export LESSHISTFILE="$XDG_STATE_HOME/less/history"


################################################################################
# Export PATH
################################################################################

#export PATH="$HOME/.nimble/bin:$PATH"

typeset -U path PATH
path=(
    "$HOME/.local/bin"
    "$HOME/.pub-cache/bin"
    "$XDG_CONFIG_HOME/emacs/bin"
    "$XDG_DATA_HOME/nimble/bin"
    "$XDG_DATA_HOME/flutter/bin"
    "$XDG_DATA_HOME/JetBrains/Toolbox/scripts"
    "$XDG_DATA_HOME/matlab/bin"
    "$XDG_DATA_HOME/Android/sdk/platform-tools/"
    "$XDG_DATA_HOME/Android/sdk/cmdline-tools/latest/bin/"
    "$VOLTA_HOME/bin"
    "$ANDROID_HOME/cmdline-tools/latest/bin"
    "$DOTNET_ROOT"
    "$DOTNET_ROOT/.dotnet/tools"
    "$PNPM_HOME"
    "$GEM_HOME/bin"
    "$GEM_HOME/ruby/3.3.0/bin/"
    "/Applications/WezTerm.app/Contents/MacOS"
    "/Applications/MATLAB_R2023b.app/bin/"
    $path
)
export PATH

# export LDFLAGS="-L/opt/homebrew/opt/llvm@12/lib"
# export CPPFLAGS="-I/opt/homebrew/opt/llvm@12/include"
. "/Users/emendoza/.local/share/cargo/env"

{{#unless archlinux}}. "$XDG_DATA_HOME/cargo/env"{{/unless}}

if [ -z "${WAYLAND_DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec dbus-run-session sway
fi

{{#if archlinux}}alias hx='helix'{{/if}}
{{#if mold-linker}}export LDFLAGS="-fuse-ld=mold -Wl,-01 -Wl,--as-needed"{{/if}}


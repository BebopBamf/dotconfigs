{{#unless laptop}}# Gnome Keyring
if [ -n "$DESKTOP_SESSION" ]; then
    eval $(gnome-keyring-daemon --start)
    export SSH_AUTH_SOCK
fi
{{/unless}}


################################################################################
# Export Default Env Variables
################################################################################

export VISUAL="emacsclient -nw -a nvim"
export EDITOR="vi"

# Configure XDG Directories
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export HISTFILE="$XDG_STATE_HOME/zsh/history"
export RANDFILE="$XDG_STATE_HOME/zsh/randfile"

################################################################################
# XDG Homes
################################################################################

# Haskell Related
export STACK_XDG=1
export GHCUP_USE_XDG_DIRS=1

# Mobile App Development
export ANDROID_HOME="$XDG_DATA_HOME/Android/Sdk"

# JAVA and related
export GRADLE_USER_HOME="$XDG_DATA_HOME/gradle"
export LEIN_HOME="$XDG_DATA_HOME/lein"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java"

# DOTNET and related
export NUGET_PACKAGES="$XDG_CACHE_HOME/NuGetPackages"
export DOTNET_CLI_HOME="$XDG_DATA_HOME/dotnet"
export DOTNET_ROOT="$XDG_DATA_HOME/dotnet"

# Rust
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"

# Go
export GOPATH="$XDG_DATA_HOME"/go

# Elixir
export MIX_XDG="true"

# ASDF
# export ASDF_CONFIG_FILE="$XDG_CONFIG_HOME/asdf/asdfrc"
# export ASDF_DIR="$XDG_DATA_HOME/asdf"
# export ASDF_DATA_DIR="$XDG_DATA_HOME/asdf"

# Proton-GE
{{#unless laptop}}export ASDF_PROTONGE_STEAM_COMPAT_DIR=".var/app/com.valvesoftware.Steam/data/Steam/compatibilitytools.d/"{{/unless}}

# Web
export ELM_HOME="$XDG_DATA_HOME/elm"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export npm_config_prefix="$HOME/.local"
export PNPM_HOME="$XDG_DATA_HOME/pnpm"
export VOLTA_HOME="$XDG_DATA_HOME/volta"
export VOLTA_FEATURE_PNPM="1"

# Caml
export OPAMROOT="$XDG_DATA_HOME/opam"
export ESY__PREFIX="$XDG_DATA_HOME/esy"

# MAFS
export AGDA_DIR="$XDG_DATA_HOME/agda"
export DOT_SAGE="$XDG_DATA_HOME/sage"

# CAS
export JULIA_DEPOT_PATH="$XDG_DATA_HOME/julia:$JULIA_DEPOT_PATH"
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME/jupyter"

# LaTeX
export TEXMFHOME="$XDG_DATA_HOME/texmf"
export TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var"
export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config"

# Random Languages
export NIMBLE_DIR="$XDG_DATA_HOME/nimble"
export PYTHONSTARTUP="/etc/python/pythonrc"
# export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"

# System Variables 
# export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export ELINKS_CONFDIR="$XDG_CONFIG_HOME/elinks"
export CHROME_EXECUTABLE="/usr/bin/chromium"
export WINEPREFIX="$XDG_DATA_HOME/wine-64"
{{#if local-helix}}export HELIX_RUNTIME="$XDG_DATA_HOME/helix"{{/if}}

# load userspace dev tools
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_DATA_DIRS="$XDG_DATA_HOME:$XDG_DATA_DIRS"
export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:$XDG_DATA_DIRS"
export XDG_DATA_DIRS="$XDG_DATA_HOME/flatpak/exports/share:$XDG_DATA_DIRS"

################################################################################
# Export PATH
################################################################################

# Configure Path Variables
typeset -U path PATH
path=(
    "$HOME/.local/bin"
    "$HOME/.pub-cache/bin"
    "$HOME/Games/scripts"
    "$XDG_CONFIG_HOME/emacs/bin"
    "$XDG_DATA_HOME/nimble/bin"
    "$XDG_DATA_HOME/flutter/bin"
    "$XDG_DATA_HOME/JetBrains/Toolbox/scripts"
    "$XDG_DATA_HOME/matlab/bin"
    "$ANDROID_HOME/cmdline-tools/latest/bin"
    "$DOTNET_ROOT"
    "$DOTNET_ROOT/.dotnet/tools"
    "$XDG_DATA_HOME/flatpak/exports/bin"
    "$VOLTA_HOME/bin"
    "$PNPM_HOME"
    "$GEM_HOME"
    $path
)
export PATH

# export PATH="$HOME/.ghcup/bin/:$PATH"
# export PATH="$HOME/.cabal/bin/:$PATH"
# export PATH="$HOME/.dotnet/tools/:$PATH"

export ZSTD_NBTHREADS={{zstd_threads}}
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus

export MOZ_DISABLE_RDD_SANDBOX=1
{{#if wayland}}
export SDL_VIDEODRIVER=wayland
export MOZ_ENABLE_WAYLAND=1
export QT_WAYLAND_FORCE_DPI=physical
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
export ECORE_EVAS_ENGINE=wayland_egl
export ELM_ENGINE=wayland_egl
export _JAVA_AWT_WM_NONREPARENTING=1
export XDG_SESSION_TYPE=wayland
export XDG_CURRENT_DESKTOP=sway
{{else}}
export MOZ_X11_EGL=1
{{/if}}

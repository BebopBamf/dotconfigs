((. (require :mason) :setup))
((. (require :mason-lspconfig) :setup)
 {:ensure_installed [:volar :rust_analyzer :fennel_language_server]})

((. (require :nvim-treesitter.configs) :setup) {:auto_install true
                                                :ensure_installed [:c
                                                                   :lua
                                                                   :fennel
                                                                   :vim
                                                                   :vimdoc
                                                                   :query
                                                                   :markdown
                                                                   :markdown_inline]
                                                :highlight {:additional_vim_regex_highlighting false
                                                            :enable true}
                                                :sync_install false})	
(local mason-registry (require :mason-registry))
(local vue-language-server-path
       (.. (: (mason-registry.get_package :vue-language-server)
              :get_install_path)
           "/node_modules/@vue/language-server"))

(local lspconfig (require :lspconfig))

(lspconfig.ts_ls.setup {:filetypes [:typescript
                                    :javascript
                                    :javascriptreact
                                    :typescriptreact
                                    :vue]
                        :init_options {:plugins [{:languages [:vue]
                                                  :location vue-language-server-path
                                                  :name "@vue/typescript-plugin"}]}})
(lspconfig.volar.setup {})

;; (fn on-attach [client]
;;   ((. (require :completion) :on_attach) client))

(lspconfig.rust_analyzer.setup {:settings
                                {:rust-analyzer
                                 {:imports
                                  {:granularity
                                   {:group :module}}
                                  :cargo
                                  {:buildScripts
                                   {:enable true}}
                                  :procMacro
                                  {:enable true}}}})

(tset (require :lspconfig.configs) :fennel_language_server
      {:default_config {:cmd [:fennel-language-server]
                        :filetypes [:fennel]
                        :root_dir (lspconfig.util.root_pattern :fnl)
                        :settings {:fennel {:diagnostics {:globals [:vim]}
                                            :workspace {:checkThirdParty false
                                                        :library (vim.api.nvim_list_runtime_paths)}}}
                        :single_file_support true}})

(lspconfig.fennel_language_server.setup {:on_attach (fn [client bufnr]
                                                      (vim.keymap.set :n
                                                                      :<leader>cf
                                                                      (fn []
                                                                        (vim.lsp.buf.format {:async true}))
                                                                      {:buffer bufnr
                                                                       :desc "Format code"
                                                                       :noremap true
                                                                       :silent true}))})	

[]

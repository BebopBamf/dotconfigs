;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Euan Mendoza"
      user-mail-address "bebopbamf@effectfree.dev")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!


(setq doom-font (font-spec :family "Iosevka Plex" :size 16)
      doom-variable-pitch-font (font-spec :family "EB Garamond" :size 16)
      doom-symbol-font (font-spec :family "Symbols Nerd Font Mono" :size 16))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;;(setq doom-theme 'doom-homage-white)
(load-theme 'modus-operandi)

(after! org-src
  (add-to-list 'org-src-block-faces '("latex" (:inherit default :extend t))))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

(add-hook! prog-mode (display-fill-column-indicator-mode 1))

;;(add-hook! text-mode (auto-fill-mode 1))

;;(after! coq
;;  (require 'agda-input)
;;  (add-hook! 'coq (lambda () (set-input-method "Agda"))))

;; (use-package! eglot
;;   :config
;;   (setq-default eglot-workspace-configuration
;;                 '((haskell (formattingProvider :fourmolu)))))

;;
;; Mail
;; 

;;(setq! +notmuch-sync-backen 'mbsync-xdg)
(setq! +notmuch-sync-backend 'mbsync-xdg)

;;
;; Treesitter
;;

(use-package! treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (setq! treesit-auto-langs '(c cpp css html java javascript json toml tsx typescript vue yaml))
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

;;
;; Rust
;;

;; (use-package! rustic
;;   :mode ("\\.rs\\'" . rust-ts-mode)
;;   :mode ("\\.rs\\'" . rustic-mode)
;;   :preface
;;   ;; HACK: `rust-mode' and `rustic' add entries to `auto-mode-alist', but
;;   ;;   package load order makes which gets precedence unpredictable. By removing
;;   ;;   them early, we rely on the `:mode' directives above to re-insert them
;;   ;;   with the correct order.
;;   (setq auto-mode-alist (assoc-delete-all "\\.rs\\'" auto-mode-alist)))

;;
;; misc
;;

(setq! epa-file-encrypt-to '("560EBD87966CE5A4"))

(use-package! websocket
  :after org-roam-ui)

;;
;; Directories
;;

(setq! xdg-documents-directory (concat (getenv "HOME") "/Documents"))
(setq! bib-location (concat xdg-documents-directory "/Research/references.bib"))
(setq! org-dir-location (concat xdg-documents-directory "/Notes/"))
(setq! org-research-notes-location (concat xdg-documents-directory "/Notes/Papers/"))
(setq! org-research-papers-location (concat xdg-documents-directory "/Research/Papers/"))


;;
;; Bibliography
;;

(setq! reftex-default-bibliography bib-location)
(setq! citar-bibliography bib-location)
(setq! citar-default-style "alphabetic")

(setq! citar-library-paths '("~/Documents/Research/Papers/")
       citar-notes-paths '("~/Documents/Notes/Papers/"))

;;
;; AUCTEX
;;

;; (use-package! latex
;;   :hook
;;   (LaTeX-mode . (lambda ()
;;                   (display-line-numbers-mode -1)
;;                   (hl-line-mode -1)
;;                   (prettify-symbols-mode -1)))
;;   :config
;;   (setq tex-fontify-script nil)
;;   (setq font-latex-fontify-script nil))

;; (after! 'latex
;;   (setq! TeX-engine-alist '(default
;;                             "Tectonic"
;;                             "tectonic -X compile -f plain %T"
;;                             "tectonic -X compile -f latex --synctex %t"
;;                             nil)))

;;(setq! tex-fontify-script nil)
;;(setq! font-latex-fontify-script nil)

(after! latex
  (add-hook! 'LaTeX-mode-hook (lambda ()
                                (display-line-numbers-mode -1)
                                (hl-line-mode -1)
                                (prettify-symbols-mode -1)))
  (setq-hook! 'LaTeX-mode-hook indent-tabs-mode 't)
  (setq! tex-fontify-script nil)
  (setq! font-latex-fontify-script nil))

;; (use-package! tex
;;   :mode (("\\.tex\\'" . LaTeX-mode))
;;   :hook
;;   ('LaTeX-mode . (lambda ()
;;                    (display-line-numbers-mode -1)
;;                    (hl-line-mode -1)
;;                    (prettify-symbols-mode -1)))
;;   :config
;;   (setq! tex-fontify-script nil)
;;   (setq! font-latex-fontify-script nil))

;;
;; Markdown
;;

(use-package! markdown-mode
  :hook
  (markdown-mode . (lambda ()
                     (display-line-numbers-mode -1)
                     (hl-line-mode -1)
                     (prettify-symbols-mode -1))))

;;
;; ORG MODE
;;

(setq! org-latex-packages-alist '())
(add-to-list 'org-latex-packages-alist '("a4paper,margin=2cm" "geometry"))
(add-to-list 'org-latex-packages-alist '("" "em-mathtools"))
;; (add-to-list 'org-latex-packages-alist '("" "mathtools"))
;; (add-to-list 'org-latex-packages-alist '("" "chunkymacros"))

(use-package! org
  :hook
  (org-mode . (lambda ()
                (display-line-numbers-mode -1)
                (hl-line-mode -1)
                (prettify-symbols-mode -1))))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;;org-latex-packages-alist '(("" "minted"))
(setq! org-directory org-dir-location
       org-startup-indented t
       org-startup-with-latex-preview t
       org-pretty-entities t
       org-pretty-entities-include-sub-superscripts nil
       org-hide-emphasis-markers t
       org-fontify-whole-heading-line t
       org-fontify-done-headline t
       org-fontify-quote-and-verse-blocks t
       org-latex-src-block-backend 'minted)

(setq-hook! 'org-mode-hook
  line-spacing 0.1)

(after! org
  (setq org-crypt-key "560EBD87966CE5A4")
  (setq org-highlight-latex-and-related '(native script entities))
  (global-org-modern-mode))

;; (setq! line-spacing 1)
;; (setq-hook! 'org-mode-hook line-spacing 2)

(use-package! org-modern
  ;; :hook
  ;; ((org-mode . org-modern-mode)
  ;;  (org-agenda-finalize . org-modern-agenda))

  :config
  ;;(setq org-auto-align-tags nil)
  (setq org-modern-star 'replace))
;; (setq org-modern-label-border 1
;;       org-modern-star 'replace))

(use-package! org-roam
  :custom
  (org-roam-directory org-dir-location)
  ;; (org-roam-capture-templates
  ;;  '(("d" "default" plain
  ;;     "%?"
  ;;     :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  ;;     :unnarrowed t)
  ;;    ("m" "maths note" plain
  ;;     "%?\n* See also\n\n* References\n"
  ;;     :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
  ;;     :unnarrowed t)))
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %?"
      :target (file+head "%<%Y-%m-%d>.org.gpg" "#+title: %<%Y-%m-%d (%A)>\n* Day Plan [/]\n\n* Reflection\n\n* Journal\n")
      :unnarrowed t)
     ("j" "journal" entry "** %<%I:%M %p>: %?"
      :target (file+head+olp
               "%<%Y-%m-%d>.org.gpg"
               "#+title: %<%Y-%m-%d (%A)>\n* Day Plan [/]\n\n* Reflection\n\n* Journal\n"
               ("Journal")))
     ("r" "reflection" item "%<%I:%M %p>: %?"
      :target (file+head+olp
               "%<%Y-%m-%d>.org.gpg"
               "#+title: %<%Y-%m-%d (%A)>\n* Day Plan [/]\n\n* Reflection\n\n* Journal\n"
               ("Reflection")))
     ("p" "day plan" checkitem "[ ] %?"
      :target (file+head+olp
               "%<%Y-%m-%d>.org.gpg"
               "#+title: %<%Y-%m-%d (%A)>\n* Day Plan [/]\n\n* Reflection\n\n* Journal\n"
               ("Day Plan")))))
  :config
  (org-roam-db-autosync-mode)
  (require 'org-roam-export))

(use-package! org-roam-ui
  :after org-roam
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t)
  (setq org-roam-ui-latex-macros '(("\\gpmul" . "\\circ")
                                   ("\\R" . "\\mathbb{R}")
                                   ("\\C" . "\\mathbb{C}")
                                   ("\\F" . "\\mathbb{F}")
                                   ("\\N" . "\\mathbb{N}")
                                   ("\\Z" . "\\mathbb{Z}")
                                   ("\\GL" . "\\mathrm{GL}")
                                   ("\\M" . "\\mathrm{M}")
                                   ("\\T" . "\\mathrm{T}")
                                   ("\\tA" . "\\mathtt{A}")
                                   ("\\tB" . "\\mathtt{B}")
                                   ("\\tC" . "\\mathtt{C}")
                                   ("\\tT" . "\\mathtt{T}")
                                   ("\\cA" . "\\mathcal{A}")
                                   ("\\cB" . "\\mathcal{B}")
                                   ("\\cC" . "\\mathcal{C}")
                                   ("\\cH" . "\\mathcal{H}")
                                   ("\\P" . "\\text{P}")
                                   ("\\NP" . "\\text{NP}")
                                   ("\\coNP" . "\\text{coNP}")
                                   ("\\AM" . "\\text{AM}")
                                   ("\\coAM" . "\\text{coAM}")
                                   ("\\GI" . "\\probsty{GI}")
                                   ("\\GIp" . "\\text{GI}")
                                   ("\\TI" . "\\probsty{TI}")
                                   ("\\TIp" . "\\text{TI}"))))

;;
;; ORG-Calendar
;;

;; (setq! plstore-cache-passphrase-for-symmetric-encryption t)

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(add-hook! meson-mode 'company-mode)

;; accept completion from copilot and fallback to company
;; (use-package! copilot
;;   :hook ((prog-mode . copilot-mode)
;;          (coq-mode . (lambda () (copilot-mode -1)))
;;          (agda2-mode . (lambda () (copilot-mode -1))))
;;   :bind (:map copilot-completion-map
;;               ("<tab>" . 'copilot-accept-completion)
;;               ("TAB" . 'copilot-accept-completion)
;;               ("C-<tab>" . 'copilot-accept-completion-by-word)
;;               ("C-TAB" . 'copilot-accept-completion-by-word)))

;; (after! copilot
;;   (setq! copilot-indent-offset-warning-disable 't))

;; (after! (evil copilot)
;;   ;; Define the custom function that either accepts the completion or does the default behavior
;;   (defun internal/copilot-tab-or-default ()
;;     (interactive)
;;     (if (and (bound-and-true-p copilot-mode)
;;              ;; Add any other conditions to check for active copilot suggestions if necessary
;;              )
;;         (copilot-accept-completion)
;;       (evil-insert 1))) ; Default action to insert a tab. Adjust as needed.

;;   ;; Bind the custom function to <tab> in Evil's insert state
;;   (evil-define-key 'insert 'global (kbd "<tab>") 'internal/copilot-tab-or-default))

;;
;; LANGUAGES
;;
;;

;;
;; C/C++
;;

(setq-hook! 'c-mode-common-hook
  indent-bars-spacing 8)

(add-hook 'c++-ts-mode-hook #'eglot-ensure)

;;
;; VueJS
;;
;; (use-package! vue-ts-mode
;;   :mode "\\.vue\\'")

;; (use-package! vue-ts-mode
;;   :hook
;;   (vue-ts-mode . lsp-deferred))

;;
;; Javascript/Typescript
;;

;;(add-hook! typescript-ts-base-mode #'eglot-ensure)
;;(add-hook! typescript-ts-mode-hook 'eglot-ensure)
;;(add-hook! tsx-ts-mode-hook 'eglot-ensure)

;; (use-package! typescript-ts-base-mode
;;   :hook
;;   (typescript-ts-base-mode . eglot-ensure))

;;(use-package! typescript-mode
;;  :mode ((".*\\.ts\\'" . typescript-mode)
;;         (".*\\.tsx\\'" . typescript-mode)))

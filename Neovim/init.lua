local function bootstrap(url)
    local name = url:gsub(".*/", "")
    local path


    path = vim.fn.stdpath("data") .. "/lazy/" .. name
    vim.opt.rtp:prepend(path)

    if vim.fn.isdirectory(path) == 0 then
        print(name .. ": installing in data dir...")

        vim.fn.system { "git", "clone", url, path }

        vim.cmd "redraw"
        print(name .. ": finished installing")
    end
end

bootstrap("https://github.com/udayvir-singh/tangerine.nvim")
bootstrap("https://github.com/udayvir-singh/hibiscus.nvim")

require "tangerine".setup {
    rtpdirs = {
        "ftplugin",
    },

    compiler = {
        verbose = false,

        hooks = { "onsave", "oninit" }
    },
}

-- require 'lazy'.setup {
--   {
--     'Olical/conjure',
--     ft = { 'clojure', 'fennel', 'lisp', 'scheme' },
--     lazy = true,
--     config = function()
--       require('conjure.main').main()
--       require('conjure.mapping')['on-filetype']()
--     end,
--     init = function()
--       -- Set configuration options here
--       vim.g['conjure#debug'] = true
--     end,
--   },
--   {
--     'nvim-telescope/telescope.nvim',
--     dependencies = {'nvim-lua/plenary.nvim'}
--   },
--   "nvim-telescope/telescope-fzf-native.nvim",
--   "github/copilot.vim",
-- }

-- require 'mason'.setup()
-- require 'mason-lspconfig'.setup {
--     ensure_installed = {
--         'volar',
--         'fennel_language_server',
--     },
-- }

-- local mason_registry = require('mason-registry')
-- local vue_language_server_path = mason_registry.get_package('vue-language-server'):get_install_path() ..
-- '/node_modules/@vue/language-server'

-- local lspconfig = require('lspconfig')
-- lspconfig.tsserver.setup {
--     init_options = {
--         plugins = {
--             {
--                 name = '@vue/typescript-plugin',
--                 location = vue_language_server_path,
--                 languages = { 'vue' },
--             },
--         },
--     },
--     filetypes = { 'typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'vue' },
-- }

--
-- No need to set `hybridMode` to `true` as it's the default value
-- lspconfig.volar.setup {}

-- require 'lspconfig.configs'.fennel_language_server = {
--     default_config = {
--         cmd = { 'fennel-language-server' },
--         filetypes = { 'fennel' },
--         single_file_support = true,
--         -- source code resides in directory `fnl/`
--         root_dir = lspconfig.util.root_pattern("fnl"),
--         settings = {
--             fennel = {
--                 workspace = {
--                     -- If you are using hotpot.nvim or aniseed,
--                     -- make the server aware of neovim runtime files.
--                     library = vim.api.nvim_list_runtime_paths(),
--                     checkThirdParty = false, -- THIS IS THE IMPORTANT LINE TO ADD
--                 },
--                 diagnostics = {
--                     globals = { 'vim' },
--                 },
--             },
--         },
--     },
-- }

-- lspconfig.fennel_language_server.setup {
--     on_attach = function(client, bufnr)
--         -- Support formatting with fnlfmt
--         vim.keymap.set('n', '<leader>cf', function()
--             vim.lsp.buf.format({ async = true })
--         end, { noremap = true, silent = true, buffer = bufnr, desc = "Format code" })
--     end,
-- }

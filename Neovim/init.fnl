(import-macros {: set! : map! : g! } :hibiscus.vim)

(g! mapleader " ")
(g! maplocalleader "\\")

(require :config.lazy)
(require :config.treesit)

(set! tabstop 4)
(set! shiftwidth 4)
(set! expandtab)
(set! smartindent)

(set! number)
(set! relativenumber)

(set! cc :81)

(set! termguicolors)
(vim.cmd.colorscheme :gruvbox)

(local builtin (require :telescope.builtin))
(map! [n :remap] :<leader>ff `(builtin.find_files))
(map! [n :remap] :<leader>ss `(builtin.live_grep))
(map! [n :remap] :<leader>sd `(builtin.live_grep))
(map! [n :remap] :<leader>bi `(builtin.buffers))

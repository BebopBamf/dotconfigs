#!/bin/sh

export AWT_TOOLKIT=MToolkit

feh --bg-scale /home/emendoza/Pictures/wallpaper.jpg &
dunst &
picom --experimental-backends -b &

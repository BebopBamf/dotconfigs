export ZSH="$XDG_DATA_HOME/oh-my-zsh"

plugins=(git)

source $ZSH/oh-my-zsh.sh

export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'

eval "$(zoxide init zsh)"
eval "$(starship init zsh)"
# eval "$(nodenv init - zsh)"

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"
# export LDFLAGS="-fuse-ld=mold -Wl, -O1 -Wl, --as-needed"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

[[ ! -r /home/emendoza/.local/share/opam/opam-init/init.zsh ]] || source /home/emendoza/.local/share/opam/opam-init/init.zsh  > /dev/null 2> /dev/null
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/emendoza/.local/share/miniforge-pypy3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/emendoza/.local/share/miniforge-pypy3/etc/profile.d/conda.sh" ]; then
        . "/home/emendoza/.local/share/miniforge-pypy3/etc/profile.d/conda.sh"
    else
        export PATH="/home/emendoza/.local/share/miniforge-pypy3/bin:$PATH"
    fi
fi
unset __conda_setup

if [ -f "/home/emendoza/.local/share/miniforge-pypy3/etc/profile.d/mamba.sh" ]; then
    . "/home/emendoza/.local/share/miniforge-pypy3/etc/profile.d/mamba.sh"
fi
# <<< conda initialize <<<

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.zsh ] && source "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.zsh

alias ls='eza --icons --git'
alias ll='eza --icons -la'
alias l='eza --icons -l'
alias cat='bat --plain'
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"
alias adb='HOME=$XDG_DATA_HOME/Android/Sdk adb'
alias edit='emacsclient -nw -a nvim'
alias mvn='mvn -gs $XDG_CONFIG_HOME/maven/settings.xml'

xdg-open-s() {
    xdg-open $1 &
}
alias open='xdg-open-s'
alias openr='xdg-open'

